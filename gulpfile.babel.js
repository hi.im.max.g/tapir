import { series, parallel } from 'gulp';
import { Pug } from './gulp/tasks/pug';
import { Style } from './gulp/tasks/style';
import { Script } from './gulp/tasks/script';
import { Image } from './gulp/tasks/image';
import { Serve } from './gulp/tasks/server';
import { WatchFiles } from './gulp/tasks/watch';
import { Fonts } from './gulp/tasks/fonts';

// Запуск сервера + watch
let RunServer = parallel(
    WatchFiles,
    Serve
);

// Dev-сборка, с запуском сервера
let Build = series(
    Fonts,
    Image,
    Style,
    Script,
    Pug
);

let Dev = series(
    Build,
    RunServer
);

// Таски
exports.default     = Dev;
exports.prod        = Build;
