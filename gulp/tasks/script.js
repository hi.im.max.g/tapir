import { src, dest }        from 'gulp';
import { path }             from '../gulp.path';
import env                  from 'gulp-environment';
import browserSync          from 'browser-sync';
import concat               from 'gulp-concat';
import sourcemaps           from 'gulp-sourcemaps';
import fileInclude          from 'gulp-file-include';
import plumber              from 'gulp-plumber';
import notify               from 'gulp-notify';
import minify               from 'gulp-minify';


// Создание основного скрипта
export const Script = (done) => {
    src(path.script.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в JS: <%= error.message %>")
        })))
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('script.min.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            ignoreFiles: ['.combo.js', '.min.js'],
            noSource: true
        }))
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.script.build)).else(dest(path.script.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}

// Создание скрипта с подключаемыми библиотеками
export const ScriptVendor = (done) => {
    src(path.script.srcVendor)
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('vendor.min.js'))
        .pipe(env.if.development(dest(path.script.build)).else(dest(path.script.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}
