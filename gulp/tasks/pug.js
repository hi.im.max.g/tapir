import { src, dest }        from 'gulp';
import { path }             from '../gulp.path';
import env                  from 'gulp-environment'
import pug                  from 'gulp-pug';
import browserSync          from 'browser-sync';
import plumber              from 'gulp-plumber';
import notify               from 'gulp-notify';

// Сборка PUG
export const Pug = (done) => {
    src(path.pug.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в шаблонизаторе: <%= error.message %>")
        })))
        .pipe(pug({
            pretty: '    ',
        }))
        .pipe(env.if.development(dest(path.pug.build)).else(dest(path.pug.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};

