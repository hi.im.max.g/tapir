import { src, dest }    from 'gulp';
import { path }         from '../gulp.path';
import env              from 'gulp-environment';
import browserSync      from 'browser-sync';

// Таск для изображений
export const Fonts = (done) => {
    src(path.fonts.src)
        .pipe(env.if.development(dest(path.fonts.build)).else(dest(path.fonts.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};
