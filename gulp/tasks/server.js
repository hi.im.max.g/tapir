import browserSync from 'browser-sync';
import { path } from '../gulp.path';

// Сервер для локальной разработки
export const  Serve = (done) => {
    browserSync.init({
        server: {
            baseDir: path.build,
            index: 'index.html'
        },
        ghostMode: true, // Дублирует поведение на всех страницах
        notify: false, // Вслывашка о старте сервера
        host: '0.0.0.0',
        port: 9000
    });

    done();
};
