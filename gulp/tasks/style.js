import { src, dest }        from 'gulp';
import { path }             from '../gulp.path';
import env                  from 'gulp-environment';
import autoprefixer         from 'gulp-autoprefixer';
import browserSync          from 'browser-sync';
import concat               from 'gulp-concat';
import cssnano              from 'gulp-cssnano';
import scss                 from 'gulp-sass';
import sourcemaps           from 'gulp-sourcemaps';
import plumber              from 'gulp-plumber';
import notify               from 'gulp-notify';


export const Style = (done) => {
    src(path.style.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в стилях: <%= error.message %>")
        })))
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(scss())
        .pipe(autoprefixer())
        .pipe(concat('style.min.css'))
        .pipe(cssnano())
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.style.build)).else(dest(path.style.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}
