import { watch } from 'gulp';
import { path } from '../gulp.path';
import { Pug } from './pug';
import { Style } from './style';
import { Script } from './script';
import { Image } from './image';
import { Fonts } from './fonts';

// Слежение за изменяемыми файлами
export const WatchFiles = (done) => {
    // watch('Путь', tasks)
    watch(path.pug.watch, Pug);
    watch(path.style.watch, Style);
    watch(path.script.watch, Script);
    watch(path.image.watch, Image);
    watch(path.fonts.watch, Fonts);
    done();
}
