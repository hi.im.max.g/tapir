export const path = {
    build: './build/',
    prod: './public/',
    pug: {
        src: [
            './src/views/pages/**/*.pug',
            './src/views/pages/*.pug',
        ],
        dist: './public/',
        build: './build/',
        watch: [
            './src/views/pages/*.pug',
            './src/views/pages/**/*.pug',
            './src/views/components/*.pug',
            './src/views/components/**/*.pug',
            './src/views/section/*.pug',
            './src/views/section/**/*.pug',
            './src/views/layouts/*.pug',
            './src/views/layouts/**/*.pug'
        ]
    },
    style: {
        src: './src/assets/scss/style.scss',
        dist: './public/assets/css/',
        build: './build/assets/css/',
        watch: [
            './src/assets/scss/*.scss',
            './src/views/pages/**/*.scss',
            './src/views/components/**/*.scss',
            './src/views/section/**/*.scss'
        ],
    },
    script: {
        src: './src/assets/js/script.js',
        dist: './public/assets/js/',
        build: './build/assets/js/',
        watch: [
            './src/assets/js/*.js',
            './src/views/pages/**/*.js',
            './src/views/components/**/*.js',
            './src/views/section/**/*.js'
        ],
    },
    image: {
        src: './src/assets/img/**/*.*',
        dist: './public/assets/img/',
        build: './build/assets/img/',
        watch: './src/assets/img/**/*.*',
    },
    fonts: {
        src: './src/assets/fonts/**/*.*',
        dist: './public/assets/fonts/',
        build: './build/assets/fonts/',
        watch: './src/assets/fonts/**/*.*',
    },
};