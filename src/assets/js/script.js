document.addEventListener('DOMContentLoaded', function() {
    let fields              = document.getElementById('payForm'),
        submit              = document.getElementById('pay'),
        cardName            = document.getElementById('name'),
        cardNumber          = document.getElementById('cardNumber'),
        cardNumSmall        = document.getElementsByClassName('card-number'),
        cardCVV             = document.getElementById('cvv');

    function getChar(event) {
        if (event.which == null) {
            if (event.keyCode < 32) return null;
            return String.fromCharCode(event.keyCode) // IE
        }
    
        if (event.which != 0 && event.charCode != 0) {
            if (event.which < 32) return null;
            return String.fromCharCode(event.which) // остальные
        }
    
        return null; // специальная клавиша
    }


    // Перебираем поля карты и запрещаем вводить буквы и специсимволы
    for(i = 0; i < cardNumSmall.length; i++ ) {
        cardNumSmall[i].onkeypress = function(e) {
            e = e || event;
            if (e.ctrlKey || e.altKey || e.metaKey) return;
            let chr = getChar(e);
            if (chr == null) return;
            if (chr < '0' || chr > '9') {
                return false;
            }
        }

        /** 
         * Удаляем класс error.
         * т.к. предыдущая функция срабатывает при нажатии, а не после,
         * делаем keyup чтобы посчитать кол-во символов и убрать error в случае его наличия
        */
        cardNumSmall[i].onkeyup = function() {
            let cardNumSmall = (this.value.length == 4);
            if(cardNumSmall) {
                this.classList.remove('error');
            } else {
                this.classList.add('error');
                return;
            }
        }
    }

    // Конкатенация полей карты в общее поле
    function concatCardNumber() {
        cardNumber = cardNumber.value = cardNumSmall[0].value + cardNumSmall[1].value + cardNumSmall[2].value + cardNumSmall[3].value;
        return cardNumber;
    }

    // Проверка имени на латиницу, а так же на наличие хотя бы 1 пробела
    let cirylic = function(name) {
        return /^[a-zA-Z]/.test(name);
    }

    // Перевод поля имени и перевод в uppercase
    cardName.onkeypress = function() {
        if(cirylic(this.value)) {
            this.value = this.value.toUpperCase()
            this.classList.remove('error')
        } else {
            this.classList.add('error');
        }
    }

    // CVV
    cardCVV.onkeypress = function(e) {
        e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var chr = getChar(e);
        if (chr == null) return;
      
        if (chr < '0' || chr > '9') {
          return false;
        }
    }
    
    cardCVV.onkeyup = function() {
        let cardNumSmall = (this.value.length == 3);
        if(cardNumSmall) {
            this.classList.remove('error');
        } else {
            this.classList.add('error');
            return;
        }
    }

    // Отправка
    submit.addEventListener('click', function(e) {
        e.preventDefault();
        concatCardNumber();

        if( fields.checkValidity() ) {
            fields.submit();
        }  else {
            alert('Форма должна быть заполнена форму')
        }
    });
});
